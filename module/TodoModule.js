var mongoose = require('../config/conDB')

var Schema = mongoose.Schema

var TodoSchema = new Schema({
    title: String,
    description: String,
},{
    collection: 'todo'
})

var TodoModule = mongoose.model('todo',TodoSchema)
module.exports = TodoModule
