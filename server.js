var experss = require('express')
var server = experss()
var path = require('path')
var UserModel = require('./module/UserModule')
var TodoModel = require('./module/TodoModule')
var bodyParser = require('body-parser')
server.use('/public', experss.static(path.join(__dirname, 'public')))
// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
server.use(bodyParser.json())

server.put('/update/:id',function(req,res,next){
    var id = req.params.id
    TodoModel.updateOne({_id:id},{title:req.body.title,description:req.body.description}).then(function(data){
        res.json({
            message:'success'
        })
    }).catch(function(err){
        res.json({
            message:'fail',
            err:err
        })
    })
})
server.delete('/delete',function(req,res,next){
    console.log(req.body.id);
    TodoModel.deleteOne({_id:req.body.id}).then(function(){
        res.json('delete success')
    }).catch(function(err){
        res.json()
    })
})
server.get('/addTodo', function (req, res, next) {
    res.sendFile(path.join(__dirname, 'addTodo.html'))
})
server.post('/addTodo', function (req, res, next) {
    TodoModel.create({ title: req.body.title, description: req.body.description }).then(function (data) {
        console.log(data);
        res.json({
            message: ' add success',
        })
    }).catch(function (err) {
        res.json(err)
    })
    console.log(req.body);
})
server.get('/todo', function (req, res, next) {
    res.sendFile(path.join(__dirname, 'todos.html'))
})
server.post('/todos',function(req,res,next){
    TodoModel.find().then(function(data){
        console.log(data);
        res.json(data)
    })
})

server.get('/login', function (req, res, next) {
    res.sendFile(path.join(__dirname, 'login.html'))
})

server.post('/login', function (req, res, next) {

    UserModel.find({ username: req.body.username, password: req.body.password }).then(function (data) {

        if (data.length == 0) {
            res.json({
                error: false,
                messagefalse: 'account exitst',
            })
        } else {
            res.json({
                error: true,
                message: 'login Success',
               
            })
        }
    }).catch(function (err) {
        console.log(err);
    })
})
server.get('/sigup', function (req, res, next) {
    res.sendfile(path.join(__dirname, 'sigup.html'))
})
server.post('/sigup', function (req, res, next) {
    UserModel.find({ username: req.body.username }).then(function (data) {
        console.log(req.body.username);
        console.log(data);

        if (data.length == 0) {
            next()
        } else {
            res.json({
                error: false,
                message: 'Account already exists, please choose another account',
            })
        }
    })
}, function (req, res, next) {
    UserModel.create({ username: req.body.username, password: req.body.password }).then(function (data) {
        res.json({
            error: true,
            message: 'Sign Up Success',
        })
    }).catch(function (err) {
        console.log(err);
    })
})
server.listen(8082, function () {
    console.log('server dang chay');
})